## About Link Shim Generator

Making this project because [this](https://www.facebook.com/notes/facebook-security/link-shim-protecting-the-people-who-use-facebook-from-malicious-urls/10150492832835766/).
This project using react, and you can deploy to firebase.

## Guide

In first after clone this project run `npm run install` to installing package dependencies.

Create `.firebaserc` file before run emulator or deploy.

file look like this:
```
{
  "projects": {
    "default": "facebook-shim"
  }
}
```
*Change `facebook-shim` to the name of your project.*

#### Commands :
- npm run login - Login to your firebase account.
- npm run deploy - Build source and deploy project to Cloud Hosting.
- npm run emulator - Run firebase emulator.
- npm run build - Build source.
- npm start - Run project locally with react.

----

## License

The Link Shim Generator is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
