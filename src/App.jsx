import React, { createRef, useState } from 'react';
import './App.css';

function App() {
  const refLinkDebug = createRef();
  const [linkOriginal, setLinkOriginal] = useState('');
  const [linkDebug, setLinkDebug] = useState('');

  const generateUrlShim = linkRedirect => {
    const link = 'https://l.facebook.com/l.php';
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    const string_length = 23;
    let makeRequest = '';
    for (let i=0; i < string_length; i++) {
      let roundNumber = Math.floor(Math.random() * chars.length);
      makeRequest += chars.substring(roundNumber, roundNumber+1);
    }
    return link + '?u=' + encodeURI(linkRedirect) + "&h="+makeRequest+"&s=1";
  };

  const handleOnLinkChange = event => {
    setLinkOriginal(event.target.value.trim());
  };

  const handleLinkSubmit = event => {
    event.preventDefault();
    // TODO: for test we disable auto add http when not avaiable
    // let fixedLink = linkOriginal;
    // if (linkOriginal && !linkOriginal.match(/^(http[s]?:\/\/)/)) {
    //   fixedLink = 'http://' + fixedLink;
    // }
    setLinkDebug(generateUrlShim(linkOriginal));
  };

  const copyLinkDebug = () => {
    if (linkDebug) {
      refLinkDebug.current.select();
      document.execCommand('copy');
    }
  };

  return (
    <div className="App">
      <div className="App-container">
        <div className="App-inner">
          <div className="App-inner-block">
            <h1>Pangkah Present</h1>
            <hr/>
            <form onSubmit={handleLinkSubmit} className="App-form">
              <input type="text" placeholder="--- LINK ---" className="form-control" value={linkOriginal} onChange={handleOnLinkChange}/>
              <button type="submit" className="form-control" disabled={!linkOriginal}>Submit</button>
            </form>
            <div className="App-form">
              <input type="text" placeholder="--- RESULT ---" className="form-control" readOnly={true} value={linkDebug} ref={refLinkDebug} onClick={copyLinkDebug}/>
              <button type="button" className="form-control" onClick={copyLinkDebug}>Copy</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
